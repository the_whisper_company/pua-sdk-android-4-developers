package com.example.puaDemo

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.TransitionDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.puaDemo.databinding.ActivityPuaDemoBinding
import com.theWhisperCompany.pua.PUA
import com.theWhisperCompany.pua.PUADelegate
import com.theWhisperCompany.pua.reasons.PUAAuthFailedReason
import com.theWhisperCompany.pua.reasons.PUAConfigFailedReason

class PUADemoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPuaDemoBinding

    private companion object {
        enum class UIState { Hidden, Visible }

        const val REQUEST_CODE_PERMISSIONS = 10
        val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }

    private lateinit var pua: PUA
    private var uiState = UIState.Hidden
    private var animDuration = 0

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() =
        ActivityCompat.requestPermissions(
            this,
            REQUIRED_PERMISSIONS,
            REQUEST_CODE_PERMISSIONS
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPuaDemoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        animDuration = resources.getInteger(android.R.integer.config_longAnimTime)

        if (allPermissionsGranted()) startPUA() else requestPermissions()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted())
                startPUA()
            else {
                Toast.makeText(
                    this,
                    "Permissions not granted",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }
    }

    private fun startPUA() =
        PUA.authenticate(this, getString(R.string.pua_api_key)) {
            pua = PUA(this, PUADemoDelegate())
            pua.previewView = binding.vCameraPreview
            pua.config()
            pua.start()

            updatePUAMessageAndStatus(
                "PUA Locked",
                "Searching face to request authentication"
            )
        }

    private fun updatePUAMessageAndStatus(message: String, status: String) {
        binding.tvPUAMessage.text = message
        binding.tvPUAStatus.text = status
    }

    private fun hideUI() {
        if (uiState == UIState.Hidden) return

        binding.ivPUALock.setImageResource(R.drawable.ic_pua_demo_locked_dark)

        // Animate camera border
        val cameraBgDrawables: Array<Drawable> = arrayOf(
            ColorDrawable(getColor(R.color.purple_200)),
            ColorDrawable(getColor(android.R.color.holo_red_dark))
        )
        val cameraBgTransition = TransitionDrawable(cameraBgDrawables)
        binding.clCameraLayout.background = cameraBgTransition
        cameraBgTransition.startTransition(500)

        binding.tvHiddenMessage.animate()
            .alpha(0f)
            .setDuration(animDuration.toLong())
            .setListener(null)

        binding.ivPUAHiddenLock.animate()
            .alpha(1f)
            .setDuration(animDuration.toLong())
            .setListener(null)

        uiState = UIState.Hidden
    }

    private fun showUI() {
        if (uiState == UIState.Visible) return

        binding.ivPUALock.setImageResource(R.drawable.ic_pua_demo_unlocked_dark)

        // Animate camera border
        val cameraBgDrawables: Array<Drawable> = arrayOf(
            ColorDrawable(getColor(android.R.color.holo_red_dark)),
            ColorDrawable(getColor(R.color.purple_200))
        )
        val cameraBgTransition = TransitionDrawable(cameraBgDrawables)
        binding.clCameraLayout.background = cameraBgTransition
        cameraBgTransition.startTransition(500)

        binding.tvHiddenMessage.animate()
            .alpha(1f)
            .setDuration(animDuration.toLong())
            .setListener(null)

        binding.ivPUAHiddenLock.animate()
            .alpha(0f)
            .setDuration(animDuration.toLong())
            .setListener(null)

        uiState = UIState.Visible
    }

    private fun onPUAConfigFailedUpdateUI(reason: PUAConfigFailedReason) =
        when (reason) {
            PUAConfigFailedReason.CameraBindingFailed ->
                updatePUAMessageAndStatus(
                    "Demo not available",
                    "Camera binding failed"
                )
            PUAConfigFailedReason.PermissionsNotGranted ->
                updatePUAMessageAndStatus(
                    "Demo not available",
                    "Permissions not granted"
                )
            PUAConfigFailedReason.NotInitialized ->
                updatePUAMessageAndStatus(
                    "Demo not available",
                    "PUA was not initialized before starting"
                )
        }

    private fun onPUAAuthFailedUpdateUI(reason: PUAAuthFailedReason) =
        when (reason) {
            PUAAuthFailedReason.FoundEyesClosed ->
                updatePUAMessageAndStatus(
                    "PUA Locked",
                    "Found eyes closed"
                )
            PUAAuthFailedReason.TrackingLost ->
                updatePUAMessageAndStatus(
                    "PUA Locked",
                    "Tracking lost"
                )
            PUAAuthFailedReason.IntruderFaceDetected ->
                updatePUAMessageAndStatus(
                    "PUA Locked",
                    "Intruder face detected"
                )
            PUAAuthFailedReason.FailedToProcessInput ->
                updatePUAMessageAndStatus(
                    "Engine error",
                    "Failed to process ML input"
                )
            PUAAuthFailedReason.FailedToLoadInput ->
                updatePUAMessageAndStatus(
                    "Engine error",
                    "Failed to load ML input"
                )
            PUAAuthFailedReason.AuthenticationRejected ->
                updatePUAMessageAndStatus(
                    "PUA Locked",
                    "Biometric authentication rejected"
                )
            PUAAuthFailedReason.NoHardware ->
                updatePUAMessageAndStatus(
                    "Hardware error",
                    "Biometric auth hardware not present"
                )
            PUAAuthFailedReason.HardwareUnavailable ->
                updatePUAMessageAndStatus(
                    "Hardware error",
                    "Biometric auth hardware not available"
                )
            PUAAuthFailedReason.NoneEnrolled ->
                updatePUAMessageAndStatus(
                    "User error",
                    "User is not enrolled in any biometric authentication"
                )
            PUAAuthFailedReason.AuthError ->
                updatePUAMessageAndStatus(
                    "PUA Locked",
                    "Authentication error"
                )
            PUAAuthFailedReason.Unknown ->
                updatePUAMessageAndStatus(
                    "Unknown error",
                    "An unknown error raised while authenticating"
                )
        }

    private fun onPUAAuthSuccessUpdateUI() = updatePUAMessageAndStatus(
        "PUA Unlocked",
        "Biometric authentication successfully"
    )

    private fun onPUAGenericErrorUpdateUI() = updatePUAMessageAndStatus(
        "Unexpected error",
        "An unexpected error occurred"
    )

    inner class PUADemoDelegate : PUADelegate {
        override fun onConfigFailed(reason: PUAConfigFailedReason) {
            onPUAConfigFailedUpdateUI(reason)
            hideUI()
        }

        override fun onAuthFailed(reason: PUAAuthFailedReason) {
            onPUAAuthFailedUpdateUI(reason)
            hideUI()
        }

        override fun onAuthSuccess() {
            onPUAAuthSuccessUpdateUI()
            showUI()
        }

        override fun onGenericError() {
            onPUAGenericErrorUpdateUI()
            hideUI()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        pua.stop()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()

        return super.onOptionsItemSelected(item)
    }
}