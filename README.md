# PUA library demo

This repo contantains an expressive demo of the PUA library. It also contains an in-depth use and installation guide.

## Instalation

**First**, create an Android project if have not done so. **Second**, import the **AAR** provided to you by _The Whisper Company_, you can easily follow the steps explained in this [official android tutorial](https://developer.android.com/studio/projects/android-library#psd-add-aar-jar-dependency).

**NOTE:** it is important to also follow the steps provided at the [official android tutorial](https://developer.android.com/studio/projects/android-library#psd-add-aar-jar-dependency), so that explicitly add the pua dependency to your project.

Next, add the following dependencies to your app level `build.gradle`:

```gradle
dependencies {
    // ...

    implementation 'com.google.mlkit:face-detection:16.1.2'

    implementation 'androidx.biometric:biometric:1.2.0-alpha03'

    implementation "androidx.camera:camera-camera2:1.1.0-alpha07"
    implementation "androidx.camera:camera-lifecycle:1.1.0-alpha07"
    implementation "androidx.camera:camera-view:1.0.0-alpha27"

    implementation "com.squareup.retrofit2:retrofit:2.9.0"
    implementation "com.squareup.retrofit2:converter-gson:2.9.0"
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.1'

    apply plugin: 'kotlin-kapt'
    def room_version = "2.3.0"
    implementation("androidx.room:room-runtime:$room_version")
    annotationProcessor "androidx.room:room-compiler:$room_version"
    kapt("androidx.room:room-compiler:$room_version")
    implementation("androidx.room:room-ktx:$room_version")
}
```

Then build your project and you are ready to use the library in your project. Keep in mind that this library only works for physical devices with biometric authentication hardware.

## About your license API KEY

Go to your `res/values` package and create a new resource file (we recommend calling this file `pua_license.xml`) with the following content:

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="pua_api_key" translatable="false">YOUR-API-KEY</string>
</resources>
```

You will later use this string resource to authenticate your PUA uses.

## Using the PUA

This guide will assume you are using the PUA within an `Activity`. However, you can use this library inside a `Fragment` as well, just be sure to:

- Be able to get the current activity that contains the fragment (PUA explicitly needs a reference to the activity that contains it to work)
- Request permissions as explained bellow
- Start the `PUA` according to the lifecycle of your fragment / activity
- Stop the `PUA` according to the lifecycle of your fragment / activity

With that clear, let's get started

### Imports

First, import the following:

```kotlin
import com.theWhisperCompany.pua.PUA
import com.theWhisperCompany.pua.PUADelegate
import com.theWhisperCompany.pua.reasons.PUAAuthFailedReason
import com.theWhisperCompany.pua.reasons.PUAConfigFailedReason
```

### Permissions

Second, the PUA needs some permissions to work.

Add the following in your `AndroidManifest.xml`:

```xml
<uses-feature android:name="android.hardware.camera.any" />
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.INTERNET"/>
```

The `CAMERA` permission needs to be requested explicitly in order for this library to work, you need to request this permission before starting to use `PUA`:

```kotlin
Manifest.permission.CAMERA
```

Your `Activity` code must at some time execute something like the following:

```kotlin
    if (allPermissionsGranted()) startPUA() else requestPermissions()
```

Where `allPermissionsGranted` and `requestPermissions` can look somewhat like this:

```
    // ...

    private companion object {
        // ...
        const val REQUEST_CODE_PERMISSIONS = 10 // You can use any code you want
        val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
        // ...
    }

    // ...

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
            ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
        }

    private fun requestPermissions() =
        ActivityCompat.requestPermissions(
            this,
            REQUIRED_PERMISSIONS,
            REQUEST_CODE_PERMISSIONS
        )
```

And of course you will have to override `onRequestPermissionsResult`:

```kotlin
    // ...

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted())
                startPUA()
            else {
                Toast.makeText(
                    this,
                    getString(R.string.permissions_not_granted),
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }
    }
```

### PUA Delegation

The PUA uses the delegation pattern to delegate the callbacks of each authentication case, any object that conforms the `PUADelegate` interface can be delegated with the callbacks of each case. For example:

```kotlin
class ExampleActivity : AppCompactActivity() {
    // ...

    inner class ExamplePUADelegate : PUADelegate {
        override fun onConfigFailed(reason: PUAConfigFailedReason) {
            onPUAConfigFailedUpdateUI(reason)
            hideUI()
        }

        override fun onAuthFailed(reason: PUAAuthFailedReason) {
            onPUAAuthFailedUpdateUI(reason)
            hideUI()
        }

        override fun onAuthSuccess() {
            onPUAAuthSuccessUpdateUI()
            showUI()
        }

        override fun onGenericError() {
            onPUAGenericErrorUpdateUI()
            hideUI()
        }
    }

    // ...
}
```

You should perform youe _lock operations_ in `onAuthFailed`, `onConfigFailed` and `onGenericError`. Your _unlock operations_ should be performed in `onAuthSuccess`.

`PUAAuthFailedReason` and `PUAConfigFailedReason` are both `enum` classes that describe the posible reasons for the corresponding callback:

```kotlin
    // ...

    private fun onPUAConfigFailedUpdateUI(reason: PUAConfigFailedReason) =
        when (reason) {
            PUAConfigFailedReason.CameraBindingFailed -> {
                // ...
                // Raised when the camera X binding fails
            }
            PUAConfigFailedReason.PermissionsNotGranted -> {
                // ...
                // Raised when PUA is started without the needed permissions
            }
            PUAConfigFailedReason.NotInitialized -> {
                // ...
                // Raised when .start() is called before .config()
            }
        }

    private fun onPUAAuthFailedUpdateUI(reason: PUAAuthFailedReason) =
        when (reason) {
            PUAAuthFailedReason.FoundEyesClosed -> {
                // ...
                // Raised when PUA detects that the user closed their eyes
            }
            PUAAuthFailedReason.TrackingLost -> {
                // ...
                // Raised when the PUA lost track of the user's face
            }
            PUAAuthFailedReason.IntruderFaceDetected -> {
                // ...
                // Raised when the PUA counts a number of faces greater than the maximum allowed
            }
            PUAAuthFailedReason.FailedToProcessInput -> {
                // ...
                // Raised when the PUA ML engine fails to process the camera input
            }
            PUAAuthFailedReason.FailedToLoadInput -> {
                // ...
                // Raised when the PUA ML engine fails to load the camera input
            }
            PUAAuthFailedReason.AuthenticationRejected -> {
                // ...
                // Raised when the biometric authentication rejects the user's identity
            }
            PUAAuthFailedReason.NoHardware -> {
                // ...
                // Raised when the current device does not have biometric auth hardware
            }
            PUAAuthFailedReason.HardwareUnavailable -> {
                // ...
                // Raised when the current device does not have biometric auth hardware available
            }
            PUAAuthFailedReason.NoneEnrolled -> {
                // ...
                // Raised when the current device is not erolled in any biometric auth method
            }
            PUAAuthFailedReason.AuthError -> {
                // ...
                // Raised when the authentication process rejects the user's identity
            }
            PUAAuthFailedReason.Unknown -> {
                // ...
                // Raised when and unkwon cause rejects the user's identity
            }
        }
```

### Starting the PUA

Once you have requested the required permissions and defined your `PUADelegate` you can start the PUA:

```kotlin
    // ...

    private lateinit var pua: PUA

    // ...

    override fun onCreate(savedInstanceState: Bundle?) {
        // ...

        if (allPermissionsGranted()) startPUA() else requestPermissions()
    }

    private fun startPUA() =
        PUA.authenticate(this, getString(R.string.pua_api_key)) {
            pua = PUA(this, ExamplePUADelegate())
            pua.config()
            pua.start()
        }
```

You will also need to at least stop the PUA according to your lifecycle (to avoid any resource leak that might occur):

```kotlin
    // ...

    override fun onDestroy() {
        super.onDestroy()
        pua.stop()
    }
```

### PUA Flow

Once started, the PUA will try to authenticate the face of the user. There are two possible outcomes: the authentication fails or success. If the authentication fails, the delegate's `onAuthFailed` method gets called. Otherwise, the delegate's `onAuthSuccess` method gets called.

The delegate's `onConfigFailed` will be called if any unexpected error occurs while configuring the PUA.

The delegate's `onGenericError` will be called if any unexpected error occurs after starting the PUA.

In general the event flow of the PUA is the following:

<br>

<p align="center">
  <img src="./resources/PUA-diagram.jpg"/>
</p>

<br>

You can access the current state of the `PUA` by using `pua.state`. This property will be either `PUAState.UserIn` or `PUAState.UserOut`, each state represents respectively whether the user is authenticated or not.

## Extra - PUA parameters TODO

You can feed each intance of `PUA` with parameters. `PUA` has a default parameter set, you can access it by `PUA.defaultConfig`.

`PUAConfig` is the class that allows you to customize parameters:

```kotlin
PUA.authenticate(this, getString(R.string.pua_api_key)) {
    val customParamters = PUAConfig()
    // ... Edit parameters

    pua = PUA(this, PUADemoDelegate(), customParamters)
    pua.config()
    pua.start()
}
```

Here is the list of parameters for _`PUAConfig`_:

- _`PUAOne.interval`_: frequency (in seconds) of tracking. default is `3` seconds.
- _`PUAOne.eyeClosedProbabilityThreshold`_: Probility threshold to consider the user's eyes closed. default is `0.8f`
- _`PUAOne.maxNumberOfFaces`_: Maximium number of faces allowed. If more faces are deteted the tracking is set to interrupted, and an `IntruderFaceDetected` reason gets raised. default is `1`.

## Extra - Getting camera preview

You can setup a `PreviewView` for getting the preview of the `PUA` camera input, you can do this by using `pua.previewView`:

```kotlin
pua.previewView = binding.vCameraPreview
```
